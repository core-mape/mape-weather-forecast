import { Component } from '@angular/core';
import { Observable } from "rxjs";
import { ForecastService } from "../../services/forecast.service";

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent {

  public forecasts$: Observable<WeatherForecast[]>;

  constructor(private forecastSrv: ForecastService) {
    this.forecasts$ = forecastSrv.getForecasts();
  }
}

export interface WeatherForecast {
  date: string,
  temperatureC: number,
  temperatureF: number,
  summary: string | null
}
