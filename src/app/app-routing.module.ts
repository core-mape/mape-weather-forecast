import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForecastComponent }    from "./pages/forecast/forecast.component";
import { HomeComponent }        from "./pages/home/home.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'forecast',
    component: ForecastComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
