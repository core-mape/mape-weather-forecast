import { Injectable }      from '@angular/core';
import { Observable, of }  from "rxjs";
import { WeatherForecast } from "../pages/forecast/forecast.component";

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  constructor() {
  }

  public getForecasts(): Observable<WeatherForecast[]> {
    const forecasts: WeatherForecast[] = [
      {
        date: "2023-07-25",
        temperatureC: 22,
        temperatureF: 71.6,
        summary: "Chilly"
      }
    ];
    return of(forecasts);
  }
}
