describe('Index Page', () => {
  it('Should have a title', () => {
    cy.visit('/');
    cy.title().should('equal', 'Weather Forecast Client');
  });
  it('Should render the navigation links', () => {
    cy.visit('/');
    cy.contains('a', 'Home');
    cy.contains('a', 'Forecast');
  });
  it('Should render the home component', () => {
    cy.visit('/');
    cy.contains('h1', 'Weather Forecast Home');
  });
  it('Navigate to forecast', () => {
    cy.visit('/');
    cy.get('a[href*="forecast"]').click();
    cy.contains('h1', 'Weather Forecast');
  });
});
