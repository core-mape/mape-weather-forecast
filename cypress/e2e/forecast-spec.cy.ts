import { Observable, of }  from "rxjs";
import { WeatherForecast } from "../../src/app/pages/forecast/forecast.component";
import { ForecastService } from "../../src/app/services/forecast.service";

describe('Forecast Page', () => {
  it('Should render the forecast component', () => {
    cy.visit('/forecast');
    cy.contains('h1', 'Weather Forecast');
  });
  it('Navigate to home', () => {
    cy.visit('/forecast');
    cy.get('a[href="/"]').click();
    cy.contains('h1', 'Weather Forecast Home');
  });
});
